# MyBatis

Please check this file for the ERD Diagram.

```shell
library-management.drawio.png
```

## Videos
### PP Class

Part 1 (Morning): <https://youtu.be/-ZhLDeeledY>

Part 2 (Afternoon): <https://youtu.be/WyKragAlbxc>

### KPS Class

Part 1 (Morning): <https://youtu.be/voDcV-TZnTc>

Part 2 (Morning): <https://youtu.be/X8c_szoKxFc>

Part 3 (Afternoon | No Audio): <https://youtu.be/AUrgTW2MMcg>

## PP Git Link

<https://gitlab.com/eam.dayan/mybatis_pp.git>
