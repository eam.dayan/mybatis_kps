drop table if exists books;
drop table if exists authors;

create table categories
(
    id   serial primary key not null,
    name varchar(50)        not null
);



create table if not exists authors
(
    id     serial primary key not null,
    name   varchar(50)        not null,
    gender varchar(1)         not null
);
create table if not exists books
(
    id          serial primary key not null,
    title       varchar(50)        not null,
    import_date timestamp,
    author_id   int                not null references authors (id)
);

-- categories: id, name
-- book_categories: book_id, category_id
create table book_categories
(
    book_id     int not null references books (id),
    category_id int not null references categories (id),
    primary key (book_id, category_id)
);