package com.ksga.databaseaccess.controller;

import com.ksga.databaseaccess.model.Book;
import com.ksga.databaseaccess.service.BookService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/")
    public ModelAndView homePage() {
        // List<Book>
        ModelAndView mv = new ModelAndView();
        mv.setViewName("index");
        System.out.println(bookService.getAllBooks());
        mv.addObject("bookList", bookService.getAllBooks());
        return mv;
    }

    @GetMapping("/{id}")
    public ModelAndView getBookById(@PathVariable Integer id) {
        Book book = bookService.findBookById(id);
        System.out.println(book);

        return new ModelAndView("index");
    }

    @GetMapping("/new")
    public String getInsertForm(
            Model model
    ) {
        model.addAttribute("book", new Book());
        return "book-insert";
    }

    @PostMapping("/insert")
    public String insertNewBook(
            @ModelAttribute Book book
    ){
        bookService.insertNewBook(book);
        return "redirect:/books/";
    }

    @GetMapping("/edit/{id}")
    public String getUpdateForm(@PathVariable Integer id, Model model){
        model.addAttribute("book", bookService.findBookById(id));
        return "book-edit";
    }

    @PostMapping("/update")
    public String updateExistingBook(@ModelAttribute Book book){
        System.out.println(book);
        bookService.updateBook(book);
        return "redirect:/books/";
    }

    @PostMapping("/delete/{id}")
    public String deleteBookById(@PathVariable Integer id){
        bookService.deleteBook(id);
        return "redirect:/books/";
    }
}
