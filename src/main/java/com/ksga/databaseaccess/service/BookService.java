package com.ksga.databaseaccess.service;

import com.ksga.databaseaccess.model.Book;

import java.util.List;

public interface BookService {

    List<Book> getAllBooks();

    Book findBookById(Integer bookId);

    void insertNewBook(Book book);

    void updateBook(Book book);

    void deleteBook(Integer id);
}
