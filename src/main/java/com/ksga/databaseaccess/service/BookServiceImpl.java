package com.ksga.databaseaccess.service;

import com.ksga.databaseaccess.model.Book;
import com.ksga.databaseaccess.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class BookServiceImpl implements BookService{

    private final BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.getAllBooksFromDatabase();
    }

    @Override
    public Book findBookById(Integer bookId) {
        return bookRepository.getBookById(bookId);
    }

    @Override
    public void insertNewBook(Book book) {
        book.setImportDate(LocalDateTime.now());
        bookRepository.insert(book);
    }

    @Override
    public void updateBook(Book book) {
        bookRepository.update(book);
    }

    @Override
    public void deleteBook(Integer id) {
        bookRepository.deleteBookById(id);
    }
}
