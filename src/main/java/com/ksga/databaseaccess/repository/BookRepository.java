package com.ksga.databaseaccess.repository;

import com.ksga.databaseaccess.model.Book;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {

    @Select("SELECT id, id as book_id, title, import_date, author_id " +
            "FROM books")
    @Result(property = "importDate", column = "import_date")
    @Result(property = "author", column = "author_id",
            one = @One(select = "com.ksga.databaseaccess.repository.AuthorRepository.getAuthorById")
    )
    @Result(property = "categories", column = "book_id",
            many = @Many(
                    select = "com.ksga.databaseaccess.repository.CategoryRepository.findCategoriesOfBookId"
            ))
    List<Book> getAllBooksFromDatabase();

    @Select("SELECT * FROM books WHERE id = #{bookId}")
    @Result(property = "importDate", column = "import_date")
    @Result(property = "author", column = "author_id",
            one = @One(select = "com.ksga.databaseaccess.repository.AuthorRepository.getAuthorById")
    )
    @Result(property = "categories", column = "book_id",
            many = @Many(
                    select = "com.ksga.databaseaccess.repository.CategoryRepository.findCategoriesOfBookId"
            ))
    Book getBookById(Integer bookId);


    @Insert("INSERT INTO books (title, import_date) " +
            "VALUES (#{book.title}, #{book.importDate})")
    void insert(@Param("book") Book book);

    @Update("UPDATE books SET title = #{book.title} " +
            "WHERE id = #{book.id}")
    void update(@Param("book") Book book);

    @Delete("DELETE FROM books WHERE id = #{bookId}")
    void deleteBookById(Integer bookId);
}
