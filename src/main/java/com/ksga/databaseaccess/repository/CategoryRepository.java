package com.ksga.databaseaccess.repository;

import com.ksga.databaseaccess.model.Category;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;

import java.util.List;

@Mapper
public interface CategoryRepository {

    @Select("SELECT c.id, c.name " +
            "FROM book_categories bc " +
            "INNER JOIN categories c on bc.category_id = c.id " +
            "WHERE book_id = #{bookId};")
    List<Category> findCategoriesOfBookId(Integer bookId);
}
