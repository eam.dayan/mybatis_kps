package com.ksga.databaseaccess.repository;

import com.ksga.databaseaccess.model.Author;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface AuthorRepository {
    // todo: do author CRUD

    @Select("SELECT * FROM authors WHERE id = #{authorId}")
    Author getAuthorById(Integer authorId);
}
